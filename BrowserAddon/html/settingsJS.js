function download(what) {
    browser.runtime.sendMessage({"download":what}).then(function(massiveCSV) {
        const blob = new Blob([massiveCSV.join("")], { type: 'text/csv' });
        const url = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = what+'.csv';
        document.body.appendChild(link);
        link.click();
        setTimeout(function() {
           URL.revokeObjectURL(url);
           document.body.removeChild(link);},10000);
      }, function(e) {
        console.log(e);
      });
}

function downloadAll() {
    download("articles");
    download("articleSeen");
    download("authors");
}

function clear() {
    document.getElementsByTagName("html")[0].classList.add("big");
    if(confirm("This will delete all stored tweets!")) {
        browser.runtime.sendMessage({"clear":true});
        alert("Please reload your twitter pages - otherwise you might end up with half-stored tweets from the pages you have currently open!");
    }
}

document.getElementById("downloadAllButton").onclick = downloadAll;
document.getElementById("clearButton").onclick = clear;