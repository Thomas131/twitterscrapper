var unsavedChanges=false;

var processedArticles = [];
function saveArticle(postlink, postedAt, userlink, text, language, nvideos, images, nreplies, nretweets, nlikes, nviews, timestamp, source) {
    if(processedArticles.includes(postlink)) {
        return;
    }
    processedArticles.push(postlink);

    var data = {
        "article": {
            [postlink]: {
                "postedAt": postedAt,
                "userlink": userlink,
                "text": text,
                "language": language,
                "nvideos": nvideos,
                "images": images,
                "nreplies": nreplies,
                "nretweets": nretweets,
                "nlikes": nlikes,
                "nviews": nviews,
                "timestamp": timestamp,
                "source": source
            }
        }
      };
    console.log(data);
    browser.runtime.sendMessage(data);
    unsavedChanges = true;
}

var processedAuthors = [];
function saveAuthor(userlink, profilepic, username, verified, timestamp, source) {
    if(processedAuthors.includes(userlink)) {
        return;
    }
    processedAuthors.push(userlink);

    var data = {
        "author": {
            [userlink]: {
                "profilepic": profilepic,
                "username": username,
                "verified": verified,
                "timestamp": timestamp,
                "source": source
            }
        }
    };
    console.log(data);
    browser.runtime.sendMessage(data);
    unsavedChanges = true;
}

var processedNodes = [];
function processArticle(a) {
    if(processedNodes.includes(a)) {
        return;
    }
    processedNodes.push(a);
    console.log(a);
    //var root = articles[0].children[0].children[0].children;
    var profilepic = a.querySelector('img[src*="https://pbs.twimg.com/profile_images/"]');
    
    var userlink = null;
    if(profilepic == null) {
        alert("couldn't fetch profile pictur and therefore User Link");
    } else {
        userlink = profilepic.parentElement;
        while(userlink && userlink.tagName.toLowerCase() != 'a') {
            userlink = userlink.parentElement;
        }

        userlink = userlink.attributes["href"].value;
        profilepic = profilepic.attributes["src"].value;
        if(userlink == document.location) {
            alert("fetching from tweet-page is not implemented yet! Skipping tweet.");
            return;
        }
    }

    var uname = a.querySelector('[data-testid="User-Name"]');
    var postedTimestamp = a.getElementsByTagName("time")[0];

    
    var postlink = postedTimestamp.parentElement;
    while(postlink && postlink.tagName.toLowerCase() != 'a') {
        postlink = postlink.parentElement;
    }

    var text = a.querySelectorAll('[lang]')[0];

    var replies = a.querySelectorAll('[aria-label*=". Reply"]')[0].innerText;
    /*if(replies == "") {
        replies = 0;
    }*/

    var retweets = a.querySelectorAll('[aria-label*=". Repost"]')[0].innerText;
    /*if(retweets == "") {
        retweets = 0;
    }*/

    var likesselector = a.querySelectorAll('[aria-label*=". Like"]');
    var likes = -1;

//    if(likesselector.length > 0) {
        likes = likesselector[0].innerText;
/*        if(likes == "") {
            likes = 0;
        }
    } else {
        likesselector = a.querySelectorAll('[href*="likes"]');
        if(likesselector.length > 0) {
            likes = likesselector[0].innerText.match("[0-9]+")[0];
        } else { //sadly this is very ambiguous
            likes = 0;
        }
    }*/

    var viewsSelector = a.querySelectorAll('[aria-label*=" views. "]');
    var views = -1;
    if(viewsSelector.length > 0) { //For some, the number of views isn't shown
        views = viewsSelector[0].innerText;
/*        if(views == "") {
            views = 0;
        }*/
    }

    var timestamp = Date.now();

    saveAuthor(userlink, profilepic, uname.children[0].innerText,a.querySelectorAll('[aria-label="Verified account"]').length==1, timestamp, window.location.href);
    saveArticle(
        postlink.attributes["href"].value,
        postedTimestamp.attributes["datetime"].value,
        userlink,
        text.innerText,
        text.attributes["lang"].value,
        a.getElementsByTagName("video").length,
        Array.from(a.querySelectorAll('img[src*="/media/"]')).map(function(i){return i.attributes["src"].value;}),
        replies,
        retweets,
        likes,
        views,
        timestamp,
        window.location.href
    );

    /*if(root.length == 3) { //You seem to be on the article page

    } else if(root.length == 2) { //eg. when searching for an article

    } else {
        alert("Unhandled condition for X scrapper: Base-Element has unexpected length. Please send Thomas the Link to the url.");
    }*/ 
}

setInterval(function(){
    articles = document.getElementsByTagName("article");
    for(var i=0;i<articles.length;i++) {
        processArticle(articles[i]);
    }

    if(unsavedChanges) {
        browser.runtime.sendMessage({saveIfChanges:true});
        console.log("Saved");
        unsavedChanges = false;
    }
}, 7000);


/*window.addEventListener('load', function load(e){
    window.removeEventListener('load', load, false);
    this.setTimeout(() => {
      run()
    }, 3000)
  }, false);
var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
    for (var i = 0; i < mutation.addedNodes.length; i++) {
        if (!(mutation.addedNodes[i] instanceof HTMLElement))
            continue;
        var articles = mutation.addedNodes[i].getElementsByTagName("article");
        for(var j=0;j<articles.length;j++) {
            processArticle(articles[j]);
        }
    }});
});
observer.observe(document.documentElement, { childList: true });*/