//We need english language
//modified from https://github.com/callahad/quick-accept-language-switcher/
chrome.webRequest.onBeforeSendHeaders.addListener(
  details => {
      for (var header of details.requestHeaders) {
        if (header.name.toLowerCase() == "accept-language") {
          header.value = "en-us";
        }
    }
    return { requestHeaders: details.requestHeaders };
  },
  { urls: ["*://x.com/*", "*://twitter.com/*", "*://*.x.com/*", "*://*.twitter.com/*"] },
  [ "blocking", "requestHeaders" ]
);





var authors;
var articles;
var authorSeen;
var articleSeen;
var changed=false;

function getAll() {
  browser.storage.local.get({"authors":{}, "articles":{}, "authorSeen":{}, "articleSeen":{}}).then(function(items) {
    authors = items.authors;
    articles = items.articles;
    articleSeen = items.articleSeen;
    authorSeen = items.authorSeen;
  }, function(e) {
    console.log(e);
  });
  changed = false;

}
getAll();

function saveAll() {
  browser.storage.local.set({ authors, articles, authorSeen, articleSeen }).then(function(items) {console.log("saved");}, function(e) {console.log(e);});
  changed = false;
}

function clear() {
  browser.storage.local.clear();
  getAll();
  changed=false;
}

function exportCSV(t) {
  var massiveCSV = [];
  var fields;
  var format; // 1: key (json_encoded);values (all json encoded); 2: key(raw);values(single raw string) - header needs to be manually added
  var variableToExport;
  if(t == "articles") {
    fields = ["postedAt", "userlink", "text", "language", "nvideos", "images", "nreplies", "nretweets", "nlikes", "nviews", "timestamp"]
    format = 1;
    variableToExport = articles;
  } else if(t == "authors") {
    fields = ["profilepic", "username", "verified", "timestamp"];
    format = 1;
    variableToExport = authors;
  } else if(t == "articleSeen") {
    variableToExport = articleSeen;
    format = 2;
    massiveCSV.push("url;source;timestamp\r\n");
  }

  if(format == 1) {
    massiveCSV.push("url;"+fields.join(";")+"\r\n");
  }

  for(let l in variableToExport) {
    if(format == 1) {
      massiveCSV.push(JSON.stringify(l));
      
      for(var fieldI=0; fieldI < fields.length; fieldI++)
        massiveCSV.push(";"+JSON.stringify(variableToExport[l][fields[fieldI]]));
      
      massiveCSV.push("\r\n");
    } else if(format == 2) {
      massiveCSV.push(l+";"+variableToExport[l]+"\r\n");
    }
  }

  console.log(massiveCSV);
  return massiveCSV;
}

function handleMessage(request, sender, sendResponse) {
  console.log(request);

  if("article" in request) {
    for(let key in request.article) {
      articleSeen[JSON.stringify(key)+";"+JSON.stringify(request.article[key].source)] = request.article[key].timestamp;
        
//      if(!(key in articles)) {
        changed=true;
        delete request.article[key].source;
        articles[key] = request.article[key];
//      }
    }
  }

  if("author" in request) {
    for(let key in request.author) {
//      authorSeen[JSON.stringify(key)+";"+JSON.stringify(request.author[key].source)] = request.author[key].timestamp;
        
//      if(!(key in authors)) {
        changed=true;
        delete request.author[key].source;
        authors[key] = request.author[key];
//      }
    }
  }

  if("saveIfChanges" in request) {
    if(changed)
      saveAll();
  }

  if("download" in request) {
    sendResponse(exportCSV(request.download));
  }

  if("clear" in request) {
    clear();
  }
}

browser.runtime.onMessage.addListener(handleMessage);