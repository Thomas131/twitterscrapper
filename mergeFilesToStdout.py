import parse
import sys

headers,data = parse.mergeFiles(sys.argv[1:])
sys.stdout.reconfigure(encoding="utf-8", newline='')
parse.exportToThisCraszyFormat(headers, data, sys.stdout)
