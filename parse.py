import json
import re
import sys

decoder = json.JSONDecoder()

def decodeLine(s):
    startIndex = 0
    json_parts = []
    while len(s[startIndex:]) > 0:
        skippingChars = [";","\r", "\n"]
        if s[startIndex] in skippingChars: #That only happens, if one of this characters appears at the beginning or afer another of those characters.
            startIndex = startIndex+1
            json_parts.append(None)
            continue
            
        try:
            json_data, json_end_index = decoder.raw_decode(s[startIndex:])
            json_parts.append(json_data)
            if len(s[startIndex+json_end_index:]) > 0:
                if s[startIndex+json_end_index] not in skippingChars:
                    print("FormatError on String "+s+" on part "+s[startIndex+json_end_index:]+" - skipping line!", file=sys.stderr)
                    return []
                startIndex = startIndex+json_end_index+1
            else:
                break
        except Exception as e:
            print("Malformed JSON on String "+s+" (" + str(e) + ") on part "+s[startIndex:]+" - skipping line!", file=sys.stderr)
            return []
    
    return json_parts


def escapeJSON(original_json_string):
    return re.sub(r';', r'\\u003b', original_json_string)

def decodeFile(file_path):
    headers = []
    data = []
    with open(file_path, 'r', encoding='utf-8') as file:
        lineNr = 0

        for line in file:
            if lineNr == 0:
                headers = line.strip().split(';')
            else:
                data.append(decodeLine(line))
            lineNr = lineNr+1
    return headers, data
#pd.DataFrame(data, columns=headers)

#merges multiple files with the same headers
def mergeFiles(arrayOfFiles):
    headers = None
    data = []
    for filepath in arrayOfFiles:
        h,d = decodeFile(filepath)
        if headers is None:
            headers = h
        else:
            if h != headers:
                print("ERROR: "+filepath+" is incompatible with the previous files!", file=sys.stderr)
                return
        data = data+d
    #remove duplicates
    for i in range(len(data)-1, -1, -1): # reverse so we can safely remove elements and the index doesn't get distorted
        for j in range(i+1,len(data)):
            if data[i] == data[j]:
                data.pop(j)
                break #the inner for loop
    return headers,data

#headers,data = mergeFiles(["C:\\articlesCollectedByP1.csv", "~/articlesCollectedByP2.csv", ".\\articlesCollectedByP3.csv", "articlesCollectedByP4.csv"])
#with open("output", 'w', encoding='utf-8') as outputfile:
#    exportToThisCraszyFormat(headers, data, outputfile)
def exportToThisCraszyFormat(headers,data,file):
    file.write(";".join(headers)+"\r\n")
    for line in data:
        file.write(";".join([escapeJSON(json.dumps(field, separators=(',', ':'))) for field in line])+"\r\n")